<?php

$nameWar = $emailWar = $passwordWar = $retypePassWar = $phoneWar = $genderWar = $fileWar = "";
$name = $email = $password = $retypepass = $phone = $gender = $file = $file_name = "";

if (isset($_POST["submit"])) {
    if (empty($_POST["name"])) {
        $nameWar = "Name is required";
    } else {
        $name = $_POST["name"];
        trim($name);
        if (!preg_match("/^[a-zA-Z\s]+$/",$name)) {
            $nameWar = "Only Character and space allowed";
        }
    }

    if (empty($_POST["email"])) {
        $emailWar = "Email is required";
    } else {
        $email = $_POST["email"];
        trim($email);
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $emailWar = "Invalid Email";
        }
    }

    if (empty($_POST["password"])) {
        $passwordWar = "Password is required";
    } else {
        $password = $_POST["password"];
        trim($password);
        if (!preg_match("/^(?=.*[0-9])(?=.*[@#$])(?=.*[A-Z]).{8,20}$/",$password)) {
            $passwordWar = "1 UpperCase 1 Numeric 1 SpecialChar Min 8 Char Required";
        }
    }

    if (empty($_POST["re_password"])) {
        $retypePassWar = "Can not be empty";
    } else {
        $retypepass = $_POST["re_password"];
        if ($password != $retypepass) {
            $retypePassWar = "Password not match";
        }
    }

    if (empty($_POST["number"])) {
        $phoneWar = "Phone number is required";
    } else {
        $phone = $_POST["number"];
        if (!preg_match("/^[6-9][0-9]{9}$/",$phone)) {
            $phoneWar = "Invalid Number";
        }
    }

    if (empty($_POST["gender"])) {
        $genderWar = "Gender is required";
    } else {
        $gender = $_POST["gender"];
    }
    
    if ($_FILES['files']['size'] == 0) {
        $fileWar = "Please Select a file";
    } else {
        if ($_FILES['files']['size'] > 2097152) {

            $fileWar='File size should less than 2 MB';
         } else {
            $file_name = $_FILES["files"]["name"];
            $file_size = $_FILES["files"]["size"];
            $file_temp = $_FILES["files"]["tmp_name"];
            $file_type = $_FILES["files"]["type"];
    
            move_uploaded_file($file_temp,"".$file_name);
        } 
    }

    

}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">    
    <title>Sign Up</title>
</head>
<body>
<form action="<?php echo htmlspecialchars("index.php");?>" method="post" enctype="multipart/form-data">
    <div>
        <div class="labl">
            <label for="name">Name : <span>*</span></label>
        </div>
        <input type="text" name="name" placeholder="Enter Name"><br><span><?php echo $nameWar; ?></span><br>
        <div class="labl">
            <label for="email">Email : <span>*</span></label><br>
        </div>
        <input type="text" name="email" placeholder="Enter Email"><br><span><?php echo $emailWar; ?></span><br>
        <div class="labl">
            <label for="password">Password : <span>* </span></label><br>
        </div class="labl">
        <input type="password" name="password" placeholder="Enter Password"><br><span><?php echo $passwordWar; ?></span><br>
        <div class="labl">
            <label for="retype">Re-Type Password : <span>* </span></label><br>
        </div>
        <input type="password" name="re_password" placeholder="Retype Passowrd"><br><span><?php echo $retypePassWar; ?></span><br>
        <div class="labl">
            <label for="number">Phone Number : <span>* </span></label><br>
        </div>
        <input type="number" name="number" placeholder="Enter Phone Number"><br><span><?php echo $phoneWar; ?></span><br>
        <div class="labl">
            <label for="gender">Gender :</label><br>
            <input type="radio" name="gender" value="male">
            <label for="male">Male</label>
            <input type="radio" name="gender" value="female">
            <label for="female">Female</label>
            <input type="radio" name="gender" value="other">
            <label for="other">Other</label><br><span><?php echo $genderWar; ?></span><br>
        </div>
        <div class="labl">
            <label for="File">Choose a File : <span>* </span></label><br>
            <input type="file" name="files"><br><span><?php echo $fileWar; ?></span><br>
        </div>
        <input type="submit" name="submit">
    </div>
</form>
</body>
</html>

<?php

if (empty($fileWar) && empty($nameWar) && empty($emailWar) && empty($passwordWar)) {
    if (empty($retypePassWar) && empty($phoneWar) && empty($genderWar)) {
        echo "<p>$name</p>";
        echo "<p>$email</p>";
        echo "<p>$phone</p>";
        echo "<p>$gender</p>";
        echo "<p>$file_name</p>";
    }
} 

?>
